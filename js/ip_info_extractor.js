$(document).ready(function () {

	var requestComplete = {};
	var now = (new Date()).getTime();
	requestComplete[now] = false;
	$.getJSON('https://smart-ip.net/geoip-json?callback=?',
   		function(data){
			requestComplete[now] = true;
    		$('#content').html(data.host);
		}).error(function() {
			unableToCheckIp()
		});
	
	setTimeout(function() {
        if (!requestComplete[now]) {
            unableToCheckIp();
        }
    }, 5000);
	
});

function unableToCheckIp() {
	$('#content').html("Unable to check IP");
}